class MainModel {
  String? description, userId, whatsapp, name, logo, color, instagram, createdAt, slug, category;
  bool? isOpen;

  MainModel(String? description, String? userId, String? whatsapp, String? name, bool? isOpen, String? logo, String? color, String? instagram,
      String? createdAt, String? slug, String? category) {
    this.description = description;
    this.userId = userId;
    this.whatsapp = whatsapp;
    this.name = name;
    this.isOpen = isOpen;
    this.logo = logo;
    this.color = color;
    this.instagram = instagram;
    this.createdAt = createdAt;
    this.slug = slug;
    this.category = category;
  }

  factory MainModel.fromJson(Map<String, dynamic> parsedJson) {
    return MainModel(
      parsedJson['description'],
      parsedJson['userId'],
      parsedJson['whatsapp'],
      parsedJson['name'],
      parsedJson['isOpen'],
      parsedJson['logo'],
      parsedJson['color'],
      parsedJson['instagram'],
      parsedJson['createdAt'],
      parsedJson['slug'],
      parsedJson['category'],
    );
  }
}
