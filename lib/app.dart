import 'package:flutter/material.dart';
import 'package:hubxapp/screens/dashboard_page.dart';
import 'package:hubxapp/screens/detail_page.dart';
import 'package:hubxapp/screens/menu_page.dart';
import 'package:hubxapp/utils/global_keys.dart';
import 'package:hubxapp/screens/schedule_page.dart';

class HubxApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tchambows Burger - Catalog',
      initialRoute: '/',
      navigatorKey: GlobalKeys.mainRoute,
      onGenerateRoute: (RouteSettings settings) {
        late Widget page;

        switch (settings.name) {
          case '/':
            page = Dashboard();
            break;
          case '/menu':
            page = Menu();
            break;
          case '/detail':
            page = Detail();
            break;
          case '/schedule':
            page = Schedule();
            break;
        }

        return PageRouteBuilder(pageBuilder: (_, __, ___) => page, transitionDuration: const Duration(seconds: 0));
      },
    );
  }
}