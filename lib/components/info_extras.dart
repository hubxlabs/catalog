import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoExtra extends StatelessWidget {
  final String? title;
  final String? subtitle;
  const InfoExtra({Key? key, this.title, this.subtitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            '$title',
            style: GoogleFonts.workSans(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: Color(0xff485460),
            ),
          ),
          Text(
            '$subtitle',
            style: GoogleFonts.workSans(
              fontSize: 14.0,
              fontWeight: FontWeight.w500,
              color: Color(0xff7f8f9f),
            ),
          ),
        ],
      ),
    );
  }
}
