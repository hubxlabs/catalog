import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonDashboard extends StatelessWidget {
  final Function onPressed;

  const ButtonDashboard({Key? key, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        minimumSize: Size(800, 54),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
      ),
      onPressed: () => onPressed(),
      child: Text(
        'Fazer meu pedido',
        style: GoogleFonts.workSans(
          fontSize: 18.0,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
      ),
    );
  }
}
