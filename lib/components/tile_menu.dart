import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TileMenu extends StatelessWidget {
  final IconData? icon;
  final String? title;
  final bool? hasTrailing;

  const TileMenu({Key? key, this.icon, this.title, this.hasTrailing}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      minLeadingWidth: 0,
      contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
      leading: Icon(
        icon!,
        color: Color(0xff7f8f9f),
      ),
      title: Text(
        title!,
        style: GoogleFonts.workSans(
          fontSize: 16.0,
          fontWeight: FontWeight.w500,
          color: Color(0xff7f8f9f),
        ),
      ),
      trailing: hasTrailing == true
          ? ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Color(0xFF00b4a5),
                minimumSize: Size(80, 30),
              ),
              onPressed: () {},
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.schedule,
                    size: 14.0,
                  ),
                  SizedBox(width: 4.0),
                  Text(
                    'Aberto',
                    style: GoogleFonts.workSans(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            )
          : SizedBox(
              height: 20.0,
            ),
    );
  }
}
