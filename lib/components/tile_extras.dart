import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TileExtras extends StatelessWidget {
  final String? title;
  final String? price;

  const TileExtras({Key? key, this.title, this.price}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
      title: Text(
        "${title!}",
        style: GoogleFonts.workSans(
          fontSize: 16.0,
          fontWeight: FontWeight.w500,
          color: Color(0xff485460),
        ),
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'R\$${price!}',
            style: GoogleFonts.workSans(
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color(0xff485460),
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.remove,
            ),
          ),
          Text(
            '0',
            style: GoogleFonts.workSans(
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color(0xff485460),
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.add,
            ),
          )
        ],
      ),
    );
  }
}
