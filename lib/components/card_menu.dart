import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CardMenu extends StatelessWidget {
  final String? title;
  final String? description;
  final String? price;
  final String? image;

  CardMenu({Key? key, this.title, this.description, this.price, this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        minVerticalPadding: 20.0,
        title: Text(
          title!,
          style: GoogleFonts.workSans(
            fontSize: 16.0,
            fontWeight: FontWeight.w700,
          ),
        ),
        subtitle: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 12.0),
              Text(
                description!,
                style: GoogleFonts.workSans(
                  fontSize: 12.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xff7f8f9f),
                ),
              ),
              SizedBox(height: 12.0),
              Text(
                'R\$ ${price!}',
                textAlign: TextAlign.start,
                style: GoogleFonts.workSans(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
        trailing: ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: image != null
              ? Image.network(
                  "${image!}",
                  fit: BoxFit.cover,
                  height: 84.0,
                  width: 84,
                )
              : SizedBox(height: 84.0, width: 84.0),
        ),
        isThreeLine: true,
      ),
    );
  }
}
