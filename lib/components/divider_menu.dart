import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DividerMenu extends StatelessWidget {
  final String? title;

  const DividerMenu({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1.0,
            color: Color(0xffe8eaed),
          ),
        ),
        color: Color(0xfff3f5f7),
      ),
      padding: EdgeInsets.only(top: 24.0, left: 16.0, bottom: 16.0, right: 16.0),
      alignment: Alignment.centerLeft,
      child: Text(
        title!,
        style: GoogleFonts.workSans(
          fontSize: 21.0,
          fontWeight: FontWeight.bold,
          color: Color(0xff485460),
        ),
      ),
    );
  }
}
