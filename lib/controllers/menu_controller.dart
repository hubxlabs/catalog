class Item {
  String? title;
  String? description;
  double? price;
  String? image;

  Item(this.title, this.description, this.price, {this.image});
}

class MenuController {
  List<Item> burgersItems = [
    Item(
      "SMASH + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão de hamburger careca de 7cm, blends smash de 100g, duas fatias de cheddar e cebola caramelizada + Batata Chips Artesanal",
      14.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/4018968/picture/medium/210222195205",
    ),
    Item(
      "SMASH + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão de hamburger careca de 7cm, 2 blends smash de 100g, duas fatias de cheddar e cebola caramelizada + Batata Chips Artesanal",
      16.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2192837/picture/medium/210222195208",
    ),
    Item(
      "CHEESEBURGER + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão com gergelim, hambúrguer artesanal de 160g, queijo, alface, tomate e molho maionese Tchambows + Batata Chips",
      17.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193331/picture/medium/210222195208",
    ),
    Item(
      "CHEESE EGG BACON + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão com gergelim, hambúrguer artesanal de 160g, bacon, ovo, queijo, alface, tomate e molho maionese Tchambows + Batata Chips Artesanal",
      19.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193479/picture/medium/210222195208",
    ),
    Item(
      "UAI + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão de hambúrguer com gergelim, carne artesanal de 160g, queijo minas derretido, crispy de couve, bacon caramelizado no melado de cana e o sensacional molho maionese de alho + Batata Chips Artesanal",
      28.5,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193461/picture/medium/210222195208",
    ),
    Item(
      "DDT + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão australiano, hambúrguer artesanal de 160g, duas fatias de cheddar, cebolas enroladas em tiras de bacon, alface americana, tomate e a saborosa maionese tchambows + Batata Chips Artesanal",
      31.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193395/picture/medium/210222195208",
    ),
    Item(
      "HOT + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão com gergelim, hambúrguer artesanal de 160g, queijo provolone, queijo muçarela, pimenta jalapeño chapeada, bacon, picles de cebola e geleia de pimenta + Batata Chips Artesanal",
      29.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193448/picture/medium/210222195208",
    ),
    Item(
      "CHICKEN + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão com gergelim, hambúrguer artesanal de frango empanado, queijo muçarela, picles de cebola, salada cremosa de repolho e molho de mostarda e mel + Batata Chips Artesanal",
      27.5,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193496/picture/medium/210222195208",
    ),
    Item(
      "PARADISE + BATATA CHIPS ARTESANAL GRÁTIS",
      "Pão com gergelim, 2 hambúrgueres artesanais de 160g, bacon crocante, duas fatias de cheddar, cebola roxa chapeada, picles de pepino e molho barbecue + Batata Chips",
      29.5,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2809616/picture/medium/210222195208",
    ),
  ];

  List<Item> drinksItems = [
    Item(
      "Agua Mineral",
      "500ml",
      3.5,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2192145/picture/medium/200830013852",
    ),
    Item(
      "Agua Mineral - Com Gás",
      "500ml",
      3.9,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2193935/picture/medium/200830022617",
    ),
    Item(
      "Coca Cola",
      "350ml",
      5.0,
    ),
    Item(
      "Coca Cola - Zero Açucar",
      "350ml",
      5.0,
    ),
    Item(
      "Guaraná Antártica",
      "350ml",
      5.0,
    ),
    Item(
      "Sprite",
      "350ml",
      5.0,
    ),
  ];

  List<Item> beerItems = [
    Item(
      "Stella Artois - Long Neck",
      "Venda e consumo proibido para menores de 18 anos. Beba com moderação.",
      7.0,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2194187/picture/medium/200830013646",
    ),
    Item(
      "Heineken - Long Neck",
      "Venda e consumo proibido para menores de 18 anos. Beba com moderação.",
      7.0,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2192141/picture/medium/200818184319",
    ),
    Item(
      "Budweiser - Long Neck",
      "Venda e consumo proibido para menores de 18 anos. Beba com moderação.",
      7.0,
      image: "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/2194193/picture/medium/200830013338",
    ),
  ];
}
