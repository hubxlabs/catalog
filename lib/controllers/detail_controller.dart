class DotMeat {
  final String? title;
  DotMeat({this.title});
}

class AdditionalInfo {
  final String? title;
  final String? price;
  AdditionalInfo({this.title, this.price});
}

class SaucesInfo {
  final String? title;
  final String? price;
  SaucesInfo({this.title, this.price});
}

class DetailController {
  final List<DotMeat> dotMeat = [
    DotMeat(title: "Ao ponto - Carne grelhada no centro rosado"),
    DotMeat(title: "Bem passada - Carne totalmente grelhada e cozinha por dentro"),
    DotMeat(title: "Mal passada - Carne selada com centro avermelhado"),
  ];

  final List<AdditionalInfo> adittionalInfo = [
    AdditionalInfo(title: "Bacon", price: "3.00"),
    AdditionalInfo(title: "Carne 180g", price: "7.00"),
    AdditionalInfo(title: "Cheddar - Fatia", price: "2.50"),
    AdditionalInfo(title: "Ovo", price: "2.50"),
  ];

  final List<SaucesInfo> saucesInfo = [
    SaucesInfo(title: "Alho", price: "2.00"),
    SaucesInfo(title: "Erva finas", price: "2.00"),
    SaucesInfo(title: "Barbecue", price: "2.00"),
  ];
}
