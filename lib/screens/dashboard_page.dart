import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hubxapp/components/button_dashboard.dart';
import 'package:hubxapp/models/main_model.dart';
import 'package:hubxapp/utils/global_keys.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Dashboard extends StatelessWidget {
  final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance.collection('tenants').where('slug', isEqualTo: 'tchambows').snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: StreamBuilder<QuerySnapshot>(
        stream: _usersStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          Widget children = Container();
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          snapshot.data?.docs.map((DocumentSnapshot document) {
            var dashboardData = MainModel.fromJson(document.data() as Map<String, dynamic>);
            children = Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(36.0),
                  child: Column(
                    children: [
                      Container(
                        height: 160,
                        padding: EdgeInsets.all(12.0),
                        width: MediaQuery.of(context).size.width,
                        child: Image.network("${dashboardData.logo}"),
                      ),
                      SizedBox(height: 4.0),
                      Container(
                        padding: EdgeInsets.all(12.0),
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          "${dashboardData.description}",
                          style: GoogleFonts.workSans(
                            color: Colors.white,
                            fontSize: 36.0,
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(12.0),
                        child: ButtonDashboard(
                          onPressed: () {
                            GlobalKeys.mainRoute.currentState?.pushNamed('/menu');
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 34.0,
                        color: dashboardData.isOpen == true ? Color(0Xff00b4a4) : Colors.red,
                        child: Center(
                          child: Text(
                            dashboardData.isOpen == true ? '● Estamos abertos' : '● Estamos fechados',
                            style: GoogleFonts.workSans(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              height: 1.2,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            );
          }).toList();
          return children;
        },
      ),
    );
  }
}
