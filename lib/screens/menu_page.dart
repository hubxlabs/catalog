import 'dart:core';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hubxapp/components/card_menu.dart';
import 'package:hubxapp/components/divider_menu.dart';
import 'package:hubxapp/components/tile_menu.dart';
import 'package:hubxapp/controllers/menu_controller.dart';
import 'package:hubxapp/utils/global_keys.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Menu extends StatefulWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  MenuController menuController = new MenuController();
  final Stream<QuerySnapshot> _tenantsStream = FirebaseFirestore.instance.collection('tenants').where('slug', isEqualTo: 'tchambows').snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: StreamBuilder<QuerySnapshot>(
        stream: _tenantsStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          Widget children = Container();
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }

          snapshot.data?.docs.map((DocumentSnapshot documentTenants) {
            children = SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    color: Colors.black,
                    height: 156,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: [
                        SizedBox(height: 36.0),
                        Container(
                          child: Image.asset(
                            'images/logo_tchambows.png',
                          ),
                          height: 90.0,
                        ),
                        Container(
                          width: 800.0,
                          height: 30.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.black,
                                ),
                                onPressed: () {},
                                child: Row(
                                  children: [
                                    Text(
                                      'Sobre a loja',
                                      style: GoogleFonts.workSans(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                    Icon(Icons.chevron_right)
                                  ],
                                ),
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.black,
                                ),
                                onPressed: () {},
                                child: Icon(Icons.search),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 800,
                    child: Column(
                      children: [
                        TileMenu(
                          title: "Hamburgueria",
                          icon: Icons.restaurant,
                          hasTrailing: true,
                        ),
                        TileMenu(
                          title: "Praça Paris, 192 - Santa Cruz Industrial, Contagem/MG",
                          icon: Icons.location_on,
                          hasTrailing: false,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            minimumSize: Size(MediaQuery.of(context).size.width, 60),
                            side: BorderSide(width: 1.0, color: Colors.black),
                            elevation: 0,
                          ),
                          onPressed: () {},
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Icon(
                                  Icons.send,
                                  size: 14.0,
                                  color: Colors.black,
                                ),
                                SizedBox(width: 4.0),
                                Text(
                                  'Selecione um endereço para entrega',
                                  style: GoogleFonts.workSans(
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 12.0),
                        DropdownButton<String>(
                          isExpanded: true,
                          value: "Burgers",
                          style: const TextStyle(color: Colors.black),
                          items: [],
                        ),
                        SizedBox(height: 12.0),
                        StreamBuilder<QuerySnapshot>(
                          stream: FirebaseFirestore.instance.collection('tenants').doc(documentTenants.id).collection('catalog').snapshots(),
                          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (!snapshot.hasData) {
                              return new Text("Loading");
                            }
                            return ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapshot.data?.docs.length,
                              itemBuilder: (BuildContext context, int index) {
                                print(snapshot.data?.docs[index].id);
                                return Column(
                                  children: [
                                    DividerMenu(title: snapshot.data?.docs[index]['name']),
                                    StreamBuilder<QuerySnapshot>(
                                      stream: FirebaseFirestore.instance
                                          .collection('tenants')
                                          .doc(documentTenants.id)
                                          .collection('catalog')
                                          .doc(snapshot.data?.docs[index].id)
                                          .collection('products')
                                          .snapshots(),
                                      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot1) {
                                        if (!snapshot1.hasData) {
                                          return new Text("Loading");
                                        }
                                        return ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: snapshot1.data?.docs.length,
                                          itemBuilder: (BuildContext context, int index) {
                                            return Column(
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    GlobalKeys.mainRoute.currentState?.pushNamed('/detail');
                                                  },
                                                  child: CardMenu(
                                                    title: snapshot1.data?.docs[index]['name'],
                                                    description: snapshot1.data?.docs[index]['description'],
                                                    price: snapshot1.data?.docs[index]['price'],
                                                    image: snapshot1.data?.docs[index]['image'],
                                                  ),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          }).toList();
          return children;
        },
      ),
    );
  }
}
