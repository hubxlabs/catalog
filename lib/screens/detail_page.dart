import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hubxapp/components/info_extras.dart';
import 'package:hubxapp/components/tile_extras.dart';
import 'package:hubxapp/controllers/detail_controller.dart';

class Detail extends StatelessWidget {
  final DetailController detailController = new DetailController();
  Detail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: 800.0,
                child: Column(
                  children: [
                    PreferredSize(
                      preferredSize: Size.fromHeight(50.0),
                      child: AppBar(
                        backgroundColor: Colors.white,
                        iconTheme: IconThemeData(
                          color: Colors.black,
                        ),
                        title: Text(
                          'Detalhe do produto',
                          style: TextStyle(color: Colors.black, fontSize: 15.0),
                        ),
                        centerTitle: true,
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Container(
                      height: 445.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.network(
                            "https://www.goomer.app/webmenu/tchambows-burger-santa-cruz-industrial/product/4018968/picture/large/210222195205",
                            height: 300.0,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.cover,
                          ),
                          Text(
                            'SMASH + BATATA CHIPS ARTESANAL GRÁTIS',
                            style: GoogleFonts.workSans(
                              fontSize: 19.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          Text(
                            'Pão de hamburger careca de 7cm, blends smash de 100g, duas fatias de cheddar e cebola caramelizada + Batata Chips Artesanal',
                            style: GoogleFonts.workSans(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff7f8f9f),
                            ),
                          ),
                          Text(
                            'R\$14,90',
                            style: GoogleFonts.workSans(
                              fontSize: 19.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 12.0),
              Container(
                width: MediaQuery.of(context).size.width,
                color: Color(0xfff3f5f7),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 12.0),
                      width: 800.0,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InfoExtra(
                                title: "Escolha o ponto da carne",
                                subtitle: "Escolha 1 opção",
                              ),
                              Container(
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xff7f8f9f),
                                    minimumSize: Size(80, 30),
                                  ),
                                  onPressed: () {},
                                  child: Text(
                                    'Obrigatório',
                                    style: GoogleFonts.workSans(
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          ListView.separated(
                            separatorBuilder: (context, index) => Divider(
                              height: 1.0,
                              color: Color(0xffe8eaed),
                            ),
                            shrinkWrap: true,
                            itemCount: detailController.dotMeat.length,
                            itemBuilder: (context, i) {
                              return ListTile(
                                contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
                                title: Text(
                                  "${detailController.dotMeat[i].title}",
                                  style: GoogleFonts.workSans(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff485460),
                                  ),
                                ),
                                trailing: Radio(
                                  value: "",
                                  groupValue: "",
                                  onChanged: (value) {},
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Container(
                      width: 800.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InfoExtra(
                            title: "Adicionais",
                            subtitle: "Escolha até 5 opções",
                          ),
                          ListView.separated(
                            separatorBuilder: (context, index) => Divider(
                              height: 1.0,
                              color: Color(0xffe8eaed),
                            ),
                            shrinkWrap: true,
                            itemCount: detailController.adittionalInfo.length,
                            itemBuilder: (context, i) {
                              return TileExtras(
                                title: detailController.adittionalInfo[i].title,
                                price: detailController.adittionalInfo[i].price,
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Container(
                      width: 800.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InfoExtra(
                            title: "Molhos",
                            subtitle: "Escolha até 5 opções",
                          ),
                          ListView.separated(
                            separatorBuilder: (context, index) => Divider(
                              height: 1.0,
                              color: Color(0xffe8eaed),
                            ),
                            shrinkWrap: true,
                            itemCount: detailController.saucesInfo.length,
                            itemBuilder: (context, i) {
                              return TileExtras(
                                title: detailController.saucesInfo[i].title,
                                price: detailController.saucesInfo[i].price,
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Container(
                      width: 800.0,
                      child: Text(
                        'Observações',
                        style: GoogleFonts.workSans(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff485460),
                        ),
                      ),
                    ),
                    SizedBox(height: 12.0),
                    Container(
                      width: 800.0,
                      child: Column(
                        children: [
                          TextFormField(
                            cursorColor: Colors.white,
                            decoration: InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'Digite as observações aqui...',
                              fillColor: Colors.white,
                              focusColor: Colors.white,
                              hoverColor: Colors.white,
                              filled: true,
                            ),
                          ),
                          SizedBox(height: 12.0),
                          Text(
                            'Converse diretamente com o estabelecimento caso queira modificar algum item. Neste campo não são aceitas modificações que podem gerar cobrança adicional.',
                            style: GoogleFonts.workSans(
                              fontSize: 12.0,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff7f8f9f),
                            ),
                          ),
                          SizedBox(height: 12.0),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
